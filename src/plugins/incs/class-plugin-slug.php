<?php
/**
 * Core class for the plugin
 */

class PluginSlug {
    protected $slug;
    protected $version;
    protected $loader;

    public function __construct() {
        // Populate properties
        $this->slug     = LOGIN_SCREEN_SLUG;
        $this->version  = LOGIN_SCREEN_VERSION;
        // Load Dependencies
        $this->loaddeps();
        $this->set_locale();
        $this->load_assets();
    }

    private function loaddeps() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'incs/class-plugin-slug-assets.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'incs/class-plugin-slug-i18n.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'incs/class-plugin-slug-loader.php';
        $this->loader = new PluginSlugLoader();
    }

    private function set_locale() {
        $i18n = new PluginSlugI18n();
        $this->loader->add_action( 'plugins_loaded', $i18n, 'load_plugin_textdomain' );
    }

    private function load_assets() {
        $utils = new PluginSlugAssets();
        $this->loader->add_action('wp_enqueue_scripts', $utils, 'enqueues');
    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }
}
