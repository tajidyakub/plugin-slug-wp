<?php
/**
 * Plugin's entry point.
 *
 * @package plugin-slug
 * @version 1.0.0
 * @link    https://bitbucket.org/tajidyakub/plugin-slug-wp
 *
 * @plugin-slug
 * Plugin Name:       Plugin Name
 * Plugin URI:        https://bitbucket.org/tajidyakub/plugin-slug-wp.git
 * Description:       A Boilerplate to create your WordPress plugin.
 * Version:           1.0.0
 * Author:            Tajid Yakub
 * Author URI:        https://tajidyakub.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-slug
 * Domain Path:       /languages
 */

/**
 * Kill the script if called directly
 */
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Define constants.
 */
define('PLUGIN_SLUG_SLUG', 'plugin-slug');
define('PLUGIN_SLUG_VERSION', '1.0.0');

// require init class
require_once plugin_dir_path( __FILE__ ) . 'incs/class-plugin-slug-init.php';

function plugin_slug_activate() {
    PluginSlugInit::activate();
}

function plugin_slug_deactivate() {
    PluginSlugInit::deactivate();
}
// activation and deactivateion hook register
register_activation_hook( __FILE__, 'plugin_slug_activate' );
register_deactivation_hook( __FILE__, 'plugin_slug_deactivate' );

// require core class
require_once plugin_dir_path( __FILE__ ) . 'incs/class-plugin-slug.php';

// require core class
function plugin_slug_execute() {
    $login_screen = new PluginSlug();
    $login_screen->run();
}

plugin_slug_execute();
